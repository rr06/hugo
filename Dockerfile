# Based on https://github.com/jojomi/docker-hugo/blob/master/Dockerfile

FROM alpine:latest

LABEL maintainer="Robert Romito <rob@robertromito.com>"
LABEL description="Rob's image for Hugo"

# Config stuff
ENV HUGO_VER=0.48 
ENV HUGO_FILE=hugo_${HUGO_VER}_Linux-64bit.tar.gz
ENV HUGO_TMP=/tmp/hugo

# Install AWS cli
RUN apk --update --no-cache add \
    python \
    py-pip \
    groff \
    less \
    mailcap \
    ca-certificates \
    tree \
    git \
    && pip install --upgrade awscli \
    && apk -v --purge del py-pip \
    && rm /var/cache/apk/* \
    && mkdir -p /tmp/hugo

# Install Hugo
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VER}/${HUGO_FILE} ${HUGO_TMP}

RUN ls -l ${HUGO_TMP} &&  tar -xf ${HUGO_TMP}/${HUGO_FILE} -C ${HUGO_TMP} \
    && mv /${HUGO_TMP}/hugo /usr/local/bin/hugo \
    && rm -rf ${HUGO_TMP}

VOLUME [ "/src" ]

WORKDIR /src

EXPOSE 1313

CMD ["/bin/sh"]