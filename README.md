# rr06's Hugo Image

## Overview

The primary purpose of this image is to provide a Hugo and AWS cli environment for publishing your Hugo site to an s3 static site bucket.

## Stating up

To start up a container, you should include a port and volume mapping.


### docker run from powershell
```powershell
docker run --rm -it `
       -v ${PWD}/src:/src `
       -v ${PWD}/dist:/dist `
       -p 1313:1313 `
       --name hugo_env `
       rr06/hugo
```

## Once inside

The following programs will be on the path
* hugo 
* aws 
